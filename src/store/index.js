import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
Vue.use(Vuex)

const store = new Vuex.Store({
    plugins: [createPersistedState()],
    state: {
        user: {},
        auth: false
    },
    getters: {
        userInfo(state) {
            return state.user
        },
        auth(state) {
            return state.auth
        }
    },
    mutations: {
        SET_USER: (state, payload) => {
            state.user = payload
        },
        SET_AUTH: (state, bool) => {
            state.auth = bool
        } 
    },
    actions: {}
});
export default store