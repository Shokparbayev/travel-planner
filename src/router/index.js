import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'

import SignUp from '@/components/Register'
import Login from '@/components/Login'

import Trips from '@/components/Trips'
import Mytrip from '@/components/Mytrip'

import Users from '@/components/Users'
Vue.use(Router)
import EditTrip from '@/components/EditTrip'
import EditUser from '@/components/EditUser'
export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/signup',
      name: 'SignUp',
      component: SignUp
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/trips',
      name: 'Trips',
      component: Trips
    },
    {
      path: '/mytrip',
      name: 'MyTrip',
      component: Mytrip
    },
    {
      path: '/users',
      name: 'Users',
      component: Users
    },
    {
      path: '/trip/edit/:id',
      name: 'EditTrip',
      component: EditTrip
    },
    {
      path: '/user/edit/:id',
      name: 'EditUser',
      component: EditUser
    }
  ],
  mode: 'history'
})
