import firebase from 'firebase'
import 'firebase/firestore'
import config from './config'
const firebaseApp = firebase.initializeApp(config)
const firestore = firebase.firestore();
const settings = {/* your settings... */ timestampsInSnapshots: true};
firestore.settings(settings);
export default firebaseApp.firestore()